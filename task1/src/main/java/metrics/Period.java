package main.java.metrics;

public class Period implements Comparable<Period>{

    String value;
    private int order;

    Period(String value) {
        this.value = value;
        if(value.equals("1W")){
            this.order = 1;
        }
        else if(value.equals("2W")){
            this.order = 2;
        }
        else if(value.equals("3W")){
            this.order = 3;
        }
        else if(value.equals("1M")){
            this.order = 4;
        }
        else if(value.equals("2M")){
            this.order = 5;
        }
        else if(value.equals("3M")){
            this.order = 6;
        }
        else if(value.equals("6M")){
            this.order = 7;
        }
        else if(value.equals("1Y")){
            this.order = 8;
        }
        else if(value.equals("2Y")){
            this.order = 9;
        }
        else if(value.equals("3Y")){
            this.order = 10;
        }
        else if(value.equals("4Y")){
            this.order = 11;
        }
        else if(value.equals("5Y")){
            this.order = 12;
        } else {
            throw new RuntimeException("");
        }
    }



    @Override
    public int compareTo(Period a){
        return Integer.compare(this.order, a.order);
    }

}
