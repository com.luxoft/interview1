package main.java.metrics;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MetricFacadeTest {

    private final MetricFacade metricFacade = new MetricFacade();

    @Test
    void getAllUsersMetricsTest() throws FileNotFoundException {
        //given
        List<String> records = getInput();

        //when
        String metrics = metricFacade.getAllUsersMetrics(records);

        //then
        assertEquals(metrics, getOutput());
    }

    private List<String> getInput() throws FileNotFoundException {
        FileInputStream fis = new FileInputStream("task1/input.txt");
        Scanner sc = new Scanner(fis);
        List<String> records = new ArrayList<>();
        while (sc.hasNextLine()) {
            records.add(sc.nextLine());
        }
        sc.close();
        return records;
    }

    private String getOutput() throws FileNotFoundException {
        FileInputStream fis = new FileInputStream("task1/output.txt");
        Scanner sc = new Scanner(fis);
        StringBuilder s = new StringBuilder();
        while (sc.hasNextLine()) {
            s.append(sc.nextLine());
            s.append("\n");
        }
        sc.close();
        return s.toString();
    }
}