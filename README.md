The getAllUsersMetrics method in the MetricFacade class takes a list of records in the format:
STAT_{FIRSTNAME}{LASTNAME}{ID}{WEBSITE}{STATISTIC-PERIOD}_{VALUE}. 

Your task is to implement the getAllUsersMetrics method so that it transforms the list of records into a single summary string in the format:

{FIRSTNAME1} {LASTNAME1}:
 {WEBSITE1}:
  {STATISTIC-PERIOD1}: {VALUE1}
  {STATISTIC-PERIOD2}: {VALUE2}
    .   
    .   
    .   

example can be found in output.txt

Records in different format should be ignored.
STAT is constant value.
{FIRSTNAME},{LASTNAME},{WEBSITE} are Strings.
{ID} is Long.
Possible values for {STATISTIC-PERIOD} are:
1W, 2W, 3W, 1M, 2M, 3M, 6M, 1Y, 2Y, 3Y, 4Y, 5Y.
Records with other values should be ignored
Possible values for {VALUE} are integers, other values should be ignored.
Users and websites should be sorted in alphabetic order, periods like below:
1W, 2W, 3W, 1M, 2M, 3M, 6M, 1Y, 2Y, 3Y, 4Y, 5Y.